import expressSwagger from 'express-swagger-generator';

import { PORT } from '../server';

export function swaggerConfiguration(app: Express.Application): void {
  const expressSwaggerDocs = expressSwagger(app);

  const options = {
    swaggerDefinition: {
      info: {
        description: 'This is a sample server',
        title: 'Swagger',
        version: '1.0.0',
      },
      host: `localhost:${PORT}`,
      basePath: '/',
      produces: [
        'application/json',
        'application/xml',
      ],
      schemes: ['http', 'https'],
      securityDefinitions: {
        JWT: {
          type: 'apiKey',
          in: 'header',
          name: 'Authorization',
          description: '',
        },
      },
    },
    basedir: __dirname,
    files: ['./routes/*.ts', './modules/**/*.ts'], // Path to the API handle folder
  };

  expressSwaggerDocs(options);
}
