import { Router } from 'express';
import multer from 'multer';

import { CategoriesController } from '../modules/cars/controllers/CategoriesController';

const categoriesRoutesFactory = (): Router => {
  const categoriesRoutes = Router();
  const categoriesController = new CategoriesController();
  const upload = multer({ dest: './tmp' });

  /**
   * @typedef Category
   * @property {string} name.required - category name
   * @property {string} description - category description (optional)
   */

  /**
   * List all categories
   * @route GET /categories
   * @group list all categories
   * @returns {object} 200 - An array of categories
   * @returns {Error}  default - Unexpected error
   */
  categoriesRoutes.get('/', categoriesController.list);

  /**
   * Add a new category to the list
   * @route POST /categories
   * @param {Category.model} category.body.required - category name
   * @group create a new category
   * @consumes application/json
   * @returns 201 - created
   */
  categoriesRoutes.post('/', categoriesController.create);

  /**
   * Add file
   * @route POST /categories/import
   * @file {File}
   * @group create multiple categories based on a CSV file
   * @consumes multipart/form-data
   * @returns 201 - created
   */
  categoriesRoutes.post('/import', upload.single('file'), categoriesController.import);
  return categoriesRoutes;
};

export { categoriesRoutesFactory };
