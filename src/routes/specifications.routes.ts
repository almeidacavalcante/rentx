import { Router } from 'express';

import { SpecificationsController } from '../modules/cars/controllers/SpecificationsController';

const specificationsRoutesFactory = (): Router => {
  const routes = Router();
  const specificationsController = new SpecificationsController();

  /**
   * @typedef Specifications
   * @property {string} name.required - specification name
   * @property {string} description - specification description (optional)
   */

  /**
   * List all specifications
   * @route GET /specifications
   * @group list all specifications
   * @returns {object} 200 - An array of specifications
   * @returns {Error}  default - Unexpected error
   */
  routes.get('/', specificationsController.list);
  /**
   * Add a new specifications to the list
   * @route POST /specifications
   * @param {Specifications.model} specifications.body.required - specifications name
   * @group create a new specifications
   * @consumes application/json
   * @returns 201 - created
   */
  routes.post('/', specificationsController.create);

  return routes;
};

export { specificationsRoutesFactory };
