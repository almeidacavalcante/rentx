import 'reflect-metadata';
import express from 'express';
import swaggerUi from 'swagger-ui-express';

import { swaggerConfiguration } from './config/swaggerConfiguration';
import './shared/container';
import connectionFactory from './database';
import { categoriesRoutesFactory } from './routes/categories.routes';
import { specificationsRoutesFactory } from './routes/specifications.routes';
import swaggerConfig from './swagger-config.json';

const server = async () => {
  await connectionFactory();
  const PORT = 3333;
  const app = express();
  swaggerConfiguration(app);
  app.use(express.json());

  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerConfig));
  app.use('/categories', categoriesRoutesFactory());
  app.use('/specifications', specificationsRoutesFactory());

  app.listen(PORT, () => console.log(`Server is listening on port: ${PORT}`));
};

server();
