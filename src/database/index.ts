import { createConnection, getConnectionOptions } from 'typeorm';

interface IOptions {
  host: string;
}

const connectionFactory = (): Promise<boolean> => new Promise((resolve, reject) => {
  getConnectionOptions().then(async (options) => {
    try {
      const newOptions = options as IOptions;
      newOptions.host = 'database'; // Essa opção deverá ser EXATAMENTE o nome dado ao service do banco de dados
      await createConnection({
        ...options,
      });
      resolve(true);
    } catch (error) {
      reject(error);
    }
  });
});

export default connectionFactory;
