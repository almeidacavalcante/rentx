import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { SpecificationsService } from '../services/SpecificationsService';

class SpecificationsController {
  public async list(request: Request, response: Response): Promise<Response> {
    const specificationsService = container.resolve(SpecificationsService);
    const all = await specificationsService.list();
    return response.status(200).json(all);
  }
  public async create(request: Request, response: Response): Promise<Response> {
    const { name, description } = request.body;
    const specificationsService = container.resolve(SpecificationsService);
    await specificationsService.create({ name, description });

    return response.status(201).send();
  }
}

export { SpecificationsController };
