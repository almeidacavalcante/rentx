import { Request, Response } from 'express';
import { container } from 'tsyringe';

import { CategoriesService } from '../services/CategoriesService';

class CategoriesController {
  public async list(request: Request, response: Response): Promise<Response> {
    const categoriesService = container.resolve(CategoriesService);
    const all = await categoriesService.list();
    return response.status(200).json(all);
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const { name, description } = request.body;
    const categoriesService = container.resolve(CategoriesService);
    await categoriesService.create({ name, description });
    return response.status(201).json();
  }

  public async import(request: Request, response: Response): Promise<Response> {
    const { file } = request;
    if (file) {
      const categoriesService = container.resolve(CategoriesService);
      await categoriesService.import(file);
      return response.send();
    }
    return response.send();
  }
}

export { CategoriesController };
