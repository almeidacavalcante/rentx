import { Specifications } from '../entities/Specifications.entity';
import { ISpecificationsRequest } from '../repositories/ISpecificationsRepository';

interface ISpecificationsService {
  list(): Promise<Specifications[]>;
  create(specifications: ISpecificationsRequest): Promise<void>;
}

export { ISpecificationsService };
