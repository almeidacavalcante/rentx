import { Category } from '../entities/Category.entity';
import { IRequest } from '../repositories/implementations/CategoriesRepository';

// TODO: Make a GenericSerivce<T>
export interface ICategoriesService {
  list(): Promise<Category[]>;
  create(category: IRequest): Promise<void>;
  import(file: Express.Multer.File): Promise<void>;
}
