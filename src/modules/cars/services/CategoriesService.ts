import csvParser from 'csv-parser';
import fs from 'fs';
import { inject, injectable } from 'tsyringe';

import { Category } from '../entities/Category.entity';
import { ICategoriesRepository } from '../repositories/ICategoriesRepository';
import { ICategoriesService } from './ICategoriesService';

interface IImportCategories {
  name: string;
  description: string;
}

@injectable()
class CategoriesService implements ICategoriesService {
  constructor(
    @inject('CategoriesRepository')
    private categoriesRepository: ICategoriesRepository,
  ) { }

  async list(): Promise<Category[]> {
    const categories = await this.categoriesRepository.list();
    return categories;
  }

  async create({ name, description }: IImportCategories): Promise<void> {
    const exists = await this.categoriesRepository.findByName(name);
    if (!exists) {
      await this.categoriesRepository.create({ name, description });
    }
  }

  async import(file: Express.Multer.File): Promise<void> {
    const categories = await this.loadCategories(file);
    categories.forEach((c) => {
      const exists = this.categoriesRepository.findByName(c.name);
      if (!exists) {
        const category = new Category(c.name, c.description, new Date());
        this.categoriesRepository.create(category);
      }
    });
    console.log('categories', categories);
  }

  private loadCategories(file: Express.Multer.File): Promise<IImportCategories[]> {
    return new Promise((resolve, reject) => {
      const categories: IImportCategories[] = [];
      const parseFile = csvParser();
      const stream = fs.createReadStream(file.path);
      stream.pipe(parseFile);
      parseFile
        .on('data', async (line) => {
          categories.push(line);
        })
        .on('end', () => {
          this.removeFile(file);
          resolve(categories);
        })
        .on('error', (err) => {
          this.removeFile(file);
          reject(err);
        });
    });
  }
  private removeFile(file: Express.Multer.File) {
    fs.promises.unlink(file.path);
  }
}

export { CategoriesService, ICategoriesService, IImportCategories };
