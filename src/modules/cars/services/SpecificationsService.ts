import { injectable, inject } from 'tsyringe';

import { Specifications } from '../entities/Specifications.entity';
import { ISpecificationsRepository, ISpecificationsRequest } from '../repositories/ISpecificationsRepository';
import { ISpecificationsService } from './ISpecificationsService';

@injectable()
class SpecificationsService implements ISpecificationsService {
  constructor(
    @inject('SpecificationsRepository')
    private specificationsRepository: ISpecificationsRepository,
  ) { }

  async list(): Promise<Specifications[]> {
    const specifications = await this.specificationsRepository.list();
    return specifications;
  }

  async create({ name, description }: ISpecificationsRequest): Promise<void> {
    const exists = await this.specificationsRepository.findByName(name);
    if (!exists) {
      await this.specificationsRepository.create({ name, description });
    }
  }
}

export { SpecificationsService };
