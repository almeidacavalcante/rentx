import { Category } from '../entities/Category.entity';
import { IImportCategories } from '../services/CategoriesService';

interface ICategoriesRepository {
  list(): Promise<Category[]>;
  create({ name, description }: IImportCategories): Promise<void>;
  findByName(name: string): Promise<Category | undefined>;
}

export { ICategoriesRepository };
