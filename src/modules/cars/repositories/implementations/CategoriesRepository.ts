import { getRepository, Repository } from 'typeorm';

import { Category } from '../../entities/Category.entity';
import { ICategoriesRepository } from '../ICategoriesRepository';

interface IRequest {
  name: string;
  description: string;
}

class CategoriesRepository implements ICategoriesRepository {
  private repository: Repository<Category>;
  constructor() {
    this.repository = getRepository(Category);
  }

  public async list(): Promise<Category[]> {
    const categories = await this.repository.find();
    return categories;
  }

  public async create({ name, description }: IRequest): Promise<void> {
    const category = this.repository.create({ description, name });
    await this.repository.save(category);
  }

  public async findByName(name: string): Promise<Category | undefined> {
    const category = this.repository.findOne({ name });
    return category;
  }
}

export { CategoriesRepository, IRequest };
