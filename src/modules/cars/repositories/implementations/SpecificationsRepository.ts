import { Repository, getRepository } from 'typeorm';

import { Specifications } from '../../entities/Specifications.entity';
import { ISpecificationsRepository, ISpecificationsRequest } from '../ISpecificationsRepository';

class SpecificationsRepository implements ISpecificationsRepository {
  private repository: Repository<Specifications>;

  constructor() {
    this.repository = getRepository(Specifications);
  }

  public async list(): Promise<Specifications[]> {
    const specifications = await this.repository.find();
    return specifications;
  }
  public async create({ name, description }: ISpecificationsRequest): Promise<void> {
    const specifications = this.repository.create({ name, description });
    await this.repository.save(specifications);
  }

  public async findByName(name: string): Promise<Specifications | undefined> {
    const specifications = this.repository.findOne({ name });
    return specifications;
  }
}

export { SpecificationsRepository };
