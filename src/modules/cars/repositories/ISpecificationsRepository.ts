import { Specifications } from '../entities/Specifications.entity';

interface ISpecificationsRequest {
  name: string;
  description: string;
}

interface ISpecificationsRepository {
  list(): Promise<Specifications[]>;
  create({ name, description }: ISpecificationsRequest): Promise<void>;
  findByName(name: string): Promise<Specifications | undefined>;
}

export { ISpecificationsRepository, ISpecificationsRequest };
