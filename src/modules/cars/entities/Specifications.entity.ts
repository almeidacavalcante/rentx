import {
  Column, CreateDateColumn, Entity, PrimaryColumn,
} from 'typeorm';
import { v4 } from 'uuid';

@Entity('specifications')
class Specifications {
  @PrimaryColumn()
  id: string;
  @Column()
  name: string;
  @Column()
  description: string;
  @CreateDateColumn()
  created_at: Date;

  constructor(
    name: string, description: string, created_at: Date,
  ) {
    this.id = v4();
    this.name = name;
    this.description = description;
    this.created_at = created_at;

    this.validate();
  }

  public validate(): void {
    // not implemented yet
  }
}

export { Specifications };
